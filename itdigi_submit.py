#!/usr/bin/env python3

import os
import sys
import time
import random
import argparse

cwd = os.getcwd()
eos = "/eos/cms/store/group/dpg_tracker_upgrade/IT_data_rates/"
path_proxy = f"{cwd}/tmp/x509up"

def main(args) :

    dataset = args.dataset
    cablemap = args.cablemap
    geometry = args.geometry
    tag = args.tag
    cmssw = args.cmssw
    xrdredirector = args.xrdredirector

    samples = open(dataset).readlines()
    njobs = len(samples)
    full_name = dataset.split("/")[-1].split(".")[0]

    path_dir = f"{cwd}/work/{full_name}/{cablemap.split('/')[-1].split('.')[0]}/{tag}/"
    path_eos = f"{eos}/{full_name}/{cablemap.split('/')[-1].split('.')[0]}/samples/{tag}/"

    for ijob in range(0, njobs) :

        sample = samples[ijob].strip()

        job = f"run_{str(ijob).zfill(3)}"
        config_file_job = f"{path_dir}/{job}/run.sh"
        condor_file_job = f"{path_dir}/{job}/condor.jds"

        os.system(f"mkdir -p {path_dir}/{job}/")
        os.system(f"mkdir -p {path_eos}/{job}/")

        os.system(f"cp {cablemap} {path_dir}/{job}/this_db.db")
        os.system(f"cp template/it-digi-exporter.dat {config_file_job}")

        os.system(f"sed -i 's|__cmssw__|{cmssw}|g' {config_file_job}")
        os.system(f"sed -i 's|__cablemap__|{cablemap}|g' {config_file_job}")
        os.system(f"sed -i 's|__xrdredirector__|{xrdredirector}|g' {config_file_job}")
        os.system(f"sed -i 's|__dataset__|{sample}|g' {config_file_job}")
        os.system(f"sed -i 's|__geometry__|{geometry}|g' {config_file_job}")

        #FIXME need to change MinBias to be xrdcp-ed with step1.root not step2.root
        xrdcp_command = fr"xrdcp link_occupancy.root root://eoscms.cern.ch/{path_eos}/{job}/"
        os.system(f"sed -i 's|__xrdcp_command__|{xrdcp_command}|g' {config_file_job}")

        os.system(f"cp template/condor.jds {condor_file_job}")
        os.system(f"sed -i 's|__process__|{full_name}|g' {condor_file_job}")
        os.system(f"sed -i 's|__tag__|{tag}|g' {condor_file_job}")
        os.system(f"sed -i 's|__path_proxy__|{path_proxy}|g' {condor_file_job}")

        print (f"cd {path_dir}/{job}")
        print (f"condor_submit condor.jds")
    print (f"cd {cwd}")

def get_argparse() :

    parser = argparse.ArgumentParser(description='sample generation')

    parser.add_argument('--xrdredirector', default = "root://eoscms.cern.ch//",
        help='Set xrootd redirector path')

    parser.add_argument('--cmssw', required = True,
        help='CMSSW release to use')

    parser.add_argument('--cablemap', required = True,
        help='Set cable map to use')

    parser.add_argument('--geometry', required = True,
        help='Set geometry to use')

    parser.add_argument('dataset',
        help='Sample to process with it digi exporter')

    parser.add_argument('-t', dest='tag', default=str(time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime())),
        help='tag for the samples')

    args = parser.parse_args()

    assert (os.path.exists(args.cablemap)) # check whether cablemap exists or not
    assert (os.path.exists(args.dataset)) # check whether dataset exists or not

    return args 

if __name__ == "__main__" :

    args = get_argparse()
    main(args)
