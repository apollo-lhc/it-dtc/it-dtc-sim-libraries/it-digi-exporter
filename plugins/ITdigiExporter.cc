// -*- C++ -*-
//
// Package:    BRIL_ITsim/ITdigiExporter
// Class:      ITdigiExporter
//
/**\class ITdigiExporter ITdigiExporter.cc BRIL_ITsim/ITdigiExporter/plugins/ITdigiExporter.cc

Description: [one line class summary]

Implementation:
[Notes on implementation]
*/
//
// Original Author:  Georg Auzinger
//         Created:  Thu, 17 Jan 2019 13:12:52 GMT
// Edits: Alexander Ruede
//
//

// system include files
// system include files
#include <fstream>
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "DataFormats/SiPixelCluster/interface/SiPixelCluster.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "FWCore/Utilities/interface/ESInputTag.h"

#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "Geometry/CommonDetUnit/interface/GeomDet.h"
#include "Geometry/Records/interface/TrackerDigiGeometryRecord.h"
#include "Geometry/CommonDetUnit/interface/PixelGeomDetUnit.h"
#include "Geometry/TrackerGeometryBuilder/interface/TrackerGeometry.h"

#include "DataFormats/Common/interface/DetSetVector.h"
#include "DataFormats/Common/interface/DetSetVectorNew.h"
#include "DataFormats/Common/interface/Handle.h"
#include "DataFormats/DetId/interface/DetId.h"
#include "DataFormats/SiPixelDetId/interface/PixelSubdetector.h"
#include "DataFormats/TrackerCommon/interface/TrackerTopology.h"
#include "DataFormats/SiPixelDigi/interface/PixelDigi.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "CommonTools/Utils/interface/TFileDirectory.h"
#include "FWCore/ServiceRegistry/interface/Service.h"


#include "CondFormats/SiPhase2TrackerObjects/interface/DTCELinkId.h"
#include "CondFormats/SiPhase2TrackerObjects/interface/TrackerDetToDTCELinkCablingMap.h"
#include "CondFormats/DataRecord/interface/TrackerDetToDTCELinkCablingMapRcd.h"

#include <TTree.h>
// class declaration
//

// If the analyzer does not use TFileService, please remove
// the template argument to the base class so the class inherits
// from  edm::one::EDAnalyzer<>
// This will improve performance in multithreaded jobs.


struct ITDigiModule {
    unsigned int module;
    unsigned int disk;
    unsigned int layer;
    bool barrel;
    uint8_t elink;
    uint8_t lpgbt;
    uint16_t dtc;
    uint64_t detid;

    std::vector<unsigned int> row;          // The pixel row number on the module
    std::vector<unsigned int> column;       // The pixel column number on the module
    std::vector<unsigned int> adc;          // ADC value for given pixel
    std::vector<unsigned int> hit;
};

struct ITDigiEvent {
    // Struct to store address and ADC information about all Digis in an event

    long int event;
    unsigned int nmodule;
    TTree * tree = NULL;
    std::vector<ITDigiModule>  modules;       // The module number

    std::vector<unsigned int> branch_detid;
    std::vector<unsigned int> branch_module;
    std::vector<unsigned int> branch_disk;
    std::vector<unsigned int> branch_layer;

    std::vector<bool> branch_barrel;
    std::vector<uint8_t> branch_elink;
    std::vector<uint8_t> branch_lpgbt;
    std::vector<uint16_t> branch_dtc;

    std::vector<std::vector<unsigned int>> branch_row;          // The pixel row number on the module
    std::vector<std::vector<unsigned int>> branch_column;       // The pixel column number on the module
    std::vector<std::vector<unsigned int>> branch_adc;          // ADC value for given pixel
    std::vector<std::vector<unsigned int>> branch_hit;

    // ADC value for given pixel
    void clear(){
        this->event = -1;
        this->nmodule = 0;
        this->modules.clear();


        this->branch_detid.clear();
        this->branch_module.clear();
        this->branch_disk.clear();
        this->branch_layer.clear();

        this->branch_barrel.clear();
        this->branch_elink.clear();
        this->branch_lpgbt.clear();
        this->branch_dtc.clear();

        this->branch_row.clear();
        this->branch_column.clear();
        this->branch_adc.clear();
	this->branch_hit.clear();
    }
    void attach_tree(TTree * tree) {
        this->tree = tree;
        tree->Branch("event",  &this->event, "event/I");
        tree->Branch("nmodule",&this->nmodule, "nmodule/I");

        tree->Branch("detid", &this->branch_detid);
        tree->Branch("module", &this->branch_module);
        tree->Branch("disk",   &this->branch_disk);
        tree->Branch("layer",  &this->branch_layer);


        tree->Branch("barrel", &this->branch_barrel);
        tree->Branch("elink",  &this->branch_elink);
        tree->Branch("lpgbt",  &this->branch_lpgbt);
        tree->Branch("dtc",    &this->branch_dtc);

        tree->Branch("row",    &this->branch_row);
        tree->Branch("column", &this->branch_column);
        tree->Branch("adc",    &this->branch_adc);
	tree->Branch("hit",    &this->branch_hit);
    }
    void serialize() {
        this->nmodule = this->modules.size();
        for(auto module : this->modules) {
            this->branch_detid.push_back(module.detid);
            this->branch_module.push_back(module.module);
            this->branch_disk.push_back(module.disk);
            this->branch_layer.push_back(module.layer);

            this->branch_barrel.push_back(module.barrel);
            this->branch_elink.push_back(module.elink);
            this->branch_lpgbt.push_back(module.lpgbt);
            this->branch_dtc.push_back(module.dtc);

            this->branch_row.push_back(module.row);
            this->branch_column.push_back(module.column);
            this->branch_adc.push_back(module.adc);
	    this->branch_hit.push_back(module.hit);
        }
        this->tree->Fill();
    }
};

class ITdigiExporter : public edm::one::EDAnalyzer<edm::one::SharedResources> {
public:
    explicit ITdigiExporter(const edm::ParameterSet&);
    ~ITdigiExporter();

    static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
    virtual void beginJob() override;
    virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
    virtual void endJob() override;

    // ----------member data ---------------------------
    edm::EDGetTokenT<edm::DetSetVector<PixelDigi>> m_tokenDigis; // digi
    edm::ESGetToken<TrackerGeometry, TrackerDigiGeometryRecord> m_tokenGeometry; // geometry
    edm::ESGetToken<TrackerTopology, TrackerTopologyRcd> m_tokenTopo; // geometry
    edm::ESGetToken<TrackerDetToDTCELinkCablingMap, TrackerDetToDTCELinkCablingMapRcd> m_tokenMap;

    // the pointers to geometry, topology and clusters
    // these are members so all functions can access them without passing as argument
    const TrackerTopology* tTopo = NULL;
    const TrackerGeometry* tkGeom = NULL;
    const edm::DetSetVector<PixelDigi>* digis = NULL;

    // File service to write ROOT output
    edm::Service<TFileService> m_fileservice;
    ITDigiEvent m_event;
    TTree * m_tree;
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
ITdigiExporter::ITdigiExporter(const edm::ParameterSet& iConfig) :
    m_tokenDigis(consumes<edm::DetSetVector<PixelDigi>>(iConfig.getParameter<edm::InputTag>("digis"))),
    m_tokenGeometry(esConsumes<TrackerGeometry, TrackerDigiGeometryRecord>(edm::ESInputTag("", "idealForDigi"))),
    m_tokenTopo(esConsumes<TrackerTopology, TrackerTopologyRcd>()),
    m_tokenMap(esConsumes<TrackerDetToDTCELinkCablingMap, TrackerDetToDTCELinkCablingMapRcd>())
{
    // TTree version 0: Save everything into one TTree
    // TODO: Organize data from different rings, detector parts
    // either into same TTree or different TTrees
    usesResource("TFileService");
    m_tree = m_fileservice->make<TTree>("Digis","Digis");
    this->m_event.attach_tree(this->m_tree);

}

ITdigiExporter::~ITdigiExporter()
{

    // do anything here that needs to be done at desctruction time
    // (e.g. close files, deallocate resources etc.)
}

//
// member functions
//

// ------------ method called for each event  ------------
void ITdigiExporter::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
    // Empty event container
    this->m_event.clear();
    this->m_event.event = iEvent.id().event();

    //get the digis - COB 26.02.19
    edm::Handle<edm::DetSetVector<PixelDigi>> tdigis;
    iEvent.getByToken(m_tokenDigis, tdigis);

    // Get the geometry
    edm::ESHandle<TrackerGeometry> tgeomHandle = iSetup.getHandle(m_tokenGeometry);
    //iSetup.getByToken(m_tokenGeometry, tgeomHandle);
    //iSetup.get<TrackerDigiGeometryRecord>().get("idealForDigi", tgeomHandle);

    // Get the topology
    edm::ESHandle<TrackerTopology> tTopoHandle = iSetup.getHandle(m_tokenTopo);
    //iSetup.get<TrackerTopologyRcd>().get(tTopoHandle);

    // Get the cabling map
    edm::ESHandle<TrackerDetToDTCELinkCablingMap> cablingMapHandle = iSetup.getHandle(m_tokenMap);
    //iSetup.get<TrackerDetToDTCELinkCablingMapRcd>().get(cablingMapHandle);
    TrackerDetToDTCELinkCablingMap const* cablingMap = cablingMapHandle.product();
    // const auto cablingMap = &iSetup.getData(m_tokenMap);

    //get the pointers to geometry and topology
    tTopo = tTopoHandle.product();
    const TrackerGeometry* tkGeom = &(*tgeomHandle);
    tkGeom = tgeomHandle.product();
    digis = tdigis.product();  //pointer to digis - COB 26.02.19

    // Loop over modules
    for (typename edm::DetSetVector<PixelDigi>::const_iterator DSVit = digis->begin(); DSVit != digis->end(); DSVit++) {

        ITDigiModule imodule;

        //get the detid
        unsigned int rawid(DSVit->detId());
        DetId detId(rawid);
        imodule.detid = rawid;


        // Determine whether it's barrel or endcap
        TrackerGeometry::ModuleType mType = tkGeom->getDetectorType(detId);
        if ((mType == TrackerGeometry::ModuleType::Ph2PXF || mType == TrackerGeometry::ModuleType::Ph2PXF3D) && detId.subdetId() == PixelSubdetector::PixelEndcap) {
            imodule.barrel = false;
        } else if ((mType == TrackerGeometry::ModuleType::Ph2PXB || mType == TrackerGeometry::ModuleType::Ph2PXB3D) && detId.subdetId() == PixelSubdetector::PixelBarrel) {
            imodule.barrel = true;
        } else {
            continue;
        }

        //obtaining location of module
        if(imodule.barrel){
            imodule.disk = tTopo->pxbLadder(detId);
            imodule.layer = tTopo->pxbLayer(detId);
            imodule.module = tTopo->pxbModule(detId);
        } else {
            imodule.disk = tTopo->pxfDisk(detId);
            imodule.layer = tTopo->pxfBlade(detId);
            imodule.module = tTopo->pxfModule(detId);
        }

        // Find the geometry of the module associated to the digi
        const GeomDetUnit* geomDetUnit(tkGeom->idToDetUnit(detId));
        if (!geomDetUnit) {
            continue;
        }

        // Loop over the digis in each module
        auto elink_ids = cablingMap->detIdToDTCELinkId(detId);
        imodule.elink = (*elink_ids.first).second.elink_id();
        imodule.lpgbt = (*elink_ids.first).second.gbtlink_id();
        imodule.dtc  = (*elink_ids.first).second.dtc_id();

        
        for (edm::DetSet<PixelDigi>::const_iterator digit = DSVit->begin(); digit != DSVit->end(); digit++) {
            imodule.row.push_back(digit->row());
            imodule.column.push_back(digit->column());
            imodule.adc.push_back(digit->adc());
	    imodule.hit.push_back(true);
        }
        this->m_event.modules.push_back(imodule);
    }

    this->m_event.serialize();
}

// ------------ method called once each job just before starting event loop  ------------
void ITdigiExporter::beginJob()
{

}

// ------------ method called once each job just after ending the event loop  ------------
void ITdigiExporter::endJob()
{

}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void ITdigiExporter::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
    //The following says we do not know what parameters are allowed so do no validation
    // Please change this to state exactly what you do use, even if it is no parameters
    edm::ParameterSetDescription desc;
    desc.setUnknown();
    descriptions.addDefault(desc);

    //Specify that only 'tracks' is allowed
    //To use, remove the default given above and uncomment below
    //ParameterSetDescription desc;
    //desc.addUntracked<edm::InputTag>("tracks","ctfWithMaterialTracks");
    //descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(ITdigiExporter);
