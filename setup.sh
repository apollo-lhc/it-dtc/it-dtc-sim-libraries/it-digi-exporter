#!/usr/bin/env bash

echo "Initializing voms proxy"

voms-proxy-init --voms cms --rfc --voms cms -valid 192:00

mkdir tmp/
cp -r /tmp/x509up_u$(id -u) tmp/x509up

echo "Copied the proxy to tmp/ directory"

