# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DEPRECATED AS OF July 2024
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This branch utilizes the cabling map CSV file as a DB which "used to be" how the plotting script in [link](https://github.com/bu-cms/dtcq/blob/master/elink_rate_study/plot_distribution.py) was handling all sorts of detector layout. Now, everything is being read on fly in the new plotting script using `detid` that we store here. Thus a lot of branches are not necessary here, check `cleanup` branch instead.

# IT digi extraction for rate studies

## Setup

```bash 
CMSSW_VERSION="CMSSW_11_3_4_patch1"
scramv1 project CMSSW "${CMSSW_VERSION}"
cd "${CMSSW_VERSION}/src"
eval `scramv1 runtime -sh`
mkdir BRIL_ITsim
cd BRIL_ITsim
git clone ssh://git@gitlab.cern.ch:7999/syuan/itdigiexporter.git
scram b -j8
```

## Running

In short:

``` bash
cmsRun itdigiexporter/python/ITdigiExporter.py \
       dataset=TTBar14TeV_10_6_0_patch2_D41_PU200 \
       output=/path/to/output_file.root
```

Some notes:
 - The dataset name must match to a file name in `./data/datasets`. The file should contain file paths to the input files for a given data set.
 - When defining a new data set name, note that the format should always be `<PhysicsProcess>_<CMSSWVersion>_<DetectorGeometryTag>_<PileUpTag>`
 - The choice of cabling map is made automatically based on the `DetectorGeometryTag` (D41 in the example above). Look at the `GEOMETRY_TO_CABLING` dictionary in the python config to see / change.
 - Note that the cabling maps are automatically loaded from `./data/cabling/`. Make sure that your desired map is available there. If you need to create a new cabling sqlite file, go [here](https://github.com/AndreasAlbert/cmssw/tree/2021-09-30_itrate_cabling_maps/CondTools/SiPhase2Tracker/test).
 - Make sure to use a CMSSW version that contains the geometry used in the MC sample being analyzed. The geometry files are periodically added / removed from CMSSW releases, so any releases that is either much newer or much older than the sample of interest will lead to problems. Additionally, CMSSW EDM files are not guaranteed to be readable by versions older than the one used to write the file.
 - Form large datasets, use crab to process. See examples in the crab/ folder.
